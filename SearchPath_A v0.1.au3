#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=0060.ico
#AutoIt3Wrapper_Res_Fileversion=0.1.0.0
#AutoIt3Wrapper_Res_LegalCopyright=Vint
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Array.au3>
#include <WindowsConstants.au3>
;~ #include <ButtonConstants.au3>
;~ #include <GUIConstantsEx.au3>
;~ #include <FontConstants.au3>
;~ #include <StaticConstants.au3>
;~ #include <WinAPI.au3>

Global $name_map, $bypass_angle, $diagonal, $StartStopInMap, $Start_X, $Start_Y, $End_X, $End_Y, $indexStart, $indexStop, $width, $height, $timer
Global $open[100][5] = [[0]], $close[100][5] = [[0]], $result
Global $check_end = False, $log = False, $best = -1
Global $ini = @ScriptDir & "\search_A.ini", $hTimerWait, $section = 'algorithmA'

FileInstall("search_A.ini", @ScriptDir & "\search_A.ini")

;~ $hTimerWait = TimerInit()
ReadingIni()
ReadMap($name_map)
$hTimerWait = TimerInit()

; ��������� ����� � �������� ������
AddOpen($Start_X, $Start_Y, 0, 0)

While $open[0][0] > 0 And Not $check_end
	$best = FindBest()
	$cur_X = $open[$best][0]
	$cur_Y = $open[$best][1]
	$cur_P = $open[$best][2]
	$cur_G = $open[$best][3]
	$cur_F = $open[$best][4]

	DelOpen($best)
	AddClose($cur_X, $cur_Y, $cur_P, $cur_G, $cur_F)
	Vicinage($cur_X, $cur_Y, $cur_G)
WEnd

If Not $check_end Then
	MsgBox(4144, "��������!", "������� ���.")
	IniWrite ($ini, $section, 'state', '2')
	Exit
EndIf

$result_min = Result()
WriteResult($result_min)
If $timer Then
	MsgBox(4096, '����� ������� ����', Int(TimerDiff($hTimerWait)) & ' ��')
EndIf

Exit


Func CalcH($X, $Y)
	Dim $End_X, $End_Y
	Local $H

	$H = (Abs($End_Y - $Y) + Abs($End_X - $X)) * 10
	Return $H
EndFunc   ;==>CalcH

Func AddOpen($X, $Y, $P, $G)
	Dim $End_X, $End_Y, $check_end
	Local $H

	$H = CalcH($X, $Y)
	$open[0][0] += 1
	If $open[0][0] > UBound($open, 1) - 1 Then
        ReDim $open[$open[0][0] + 100][5]
    EndIf

    $open[$open[0][0]][0] = $X
    $open[$open[0][0]][1] = $Y
    $open[$open[0][0]][2] = $P
	$open[$open[0][0]][3] = $G
	$open[$open[0][0]][4] = $G + $H

	If $X = $End_X And $Y = $End_Y Then $check_end = True
	If $log Then ConsoleWrite('Add open:  '& $X &':'& $Y &':'& $P &':'& $G + $H &'    G:'& $G &' $H:'& $H & @CRLF)
EndFunc   ;==>AddOpen

Func AddClose($X, $Y, $P, $G, $F)
	;Local $H

	$H = CalcH($X, $Y)
	$close[0][0] += 1
	If $close[0][0] > UBound($close, 1) - 1 Then
        ReDim $close[$close[0][0] + 100][5]
    EndIf

    $close[$close[0][0]][0] = $X
    $close[$close[0][0]][1] = $Y
    $close[$close[0][0]][2] = $P
	$close[$close[0][0]][3] = $G
	;$close[$close[0][0]][4] = $G + $H
	$close[$close[0][0]][4] = $F

	;If $log Then ConsoleWrite('Add close:  '& $X &':'& $Y &':'& $P &':'& $G + $H &'    G:'& $G &' $H:'& $H & @CRLF)
	If $log Then ConsoleWrite('Add close:  '& $X &':'& $Y &':'& $P &':'& $F &'    G:'& $G &' $H:'& $H & @CRLF)
EndFunc   ;==>AddClose

Func DelOpen($ind)
	If $log Then ConsoleWrite('del open:  '& $open[$ind][0] &':'& $open[$ind][1] &':'& $open[$ind][2] &':'& $open[$ind][4] & @CRLF)

	If $open[0][0] > 1 Then
		For $i = $ind To $open[0][0] - 1
			$open[$i][0] = $open[$i+1][0]
			$open[$i][1] = $open[$i+1][1]
			$open[$i][2] = $open[$i+1][2]
			$open[$i][3] = $open[$i+1][3]
			$open[$i][4] = $open[$i+1][4]
		Next
	EndIf
	$open[$open[0][0]][0] = 0
	$open[$open[0][0]][1] = 0
	$open[$open[0][0]][2] = 0
	$open[$open[0][0]][3] = 0
	$open[$open[0][0]][4] = 0
	$open[0][0] -= 1
EndFunc   ;==>DelOpen

Func CheckStateSpis($X, $Y)
	Dim $width, $height
	Local $opened[2], $closed[2]
	Local $aStateSpis[5] = [True, False, False, -1, -1]	; Filed, closed, opened, index opened, index closed

	If $X < 0 Or $Y < 0 Or $X >=$width Or $Y >= $height Then
		$aStateSpis[0] = False
		Return $aStateSpis
	EndIf

	$closed = CheckClose($X, $Y)
	$aStateSpis[1] = $closed[0]
	$aStateSpis[4] = $closed[1]
	If $aStateSpis[1] Then Return $aStateSpis

	$opened = CheckOpen($X, $Y)
	$aStateSpis[2] = $opened[0]
	$aStateSpis[3] = $opened[1]
;~ 	_ArrayDisplay($aStateSpis)
	Return $aStateSpis
EndFunc   ;==>CheckStateSpis

Func CheckOpen($X, $Y)
	Local $opened[2] = [False, -1]

	For $i = 1 To $open[0][0]
		If $X = $open[$i][0] And $Y = $open[$i][1] Then
			$opened[0] = True
			$opened[1] = $i
			ExitLoop
		EndIf
	Next
	Return $opened
EndFunc   ;==>CheckOpen

Func CheckClose($X, $Y)
	Local $closed[2] = [False, -1]

	For $i = 1 To $close[0][0]
		If $X = $close[$i][0] And $Y = $close[$i][1] Then
			$closed[0] = True
			$closed[1] = $i
			ExitLoop
		EndIf
	Next
	Return $closed
EndFunc   ;==>CheckClose

Func Reading($X, $Y)
	Dim $width, $height, $map
	Local $closed[2]

	;If $log Then ConsoleWrite('$X/$Y $width/$height:  '& $X &'/'& $Y &'    '& $width &'/'& $height & @CRLF)
	$closed = CheckClose($X, $Y)
	If $X < 0 Or $Y < 0 Or $X >= $width Or $Y >= $height Or $closed[0] Then Return 1

	;If $log Then ConsoleWrite('read mapping' & @CRLF)
	Return $map[$Y][$X]
EndFunc   ;==>Reading

Func Vicinage($X, $Y, $G_cur)
	Orto($X, $Y, 0, -1, 1, $G_cur)		; �����, 1
	Diag($X, $Y, 1, -1, 2, $G_cur)		; �����-������, 2
	Orto($X, $Y, 1, 0, 3, $G_cur)		; ������
	Diag($X, $Y, 1, 1, 4, $G_cur)		; ������-����
	Orto($X, $Y, 0, 1, 5, $G_cur)		; ����
	Diag($X, $Y, -1, 1, 6, $G_cur)		; �����-����
	Orto($X, $Y, -1, 0, 7, $G_cur)		; �����
	Diag($X, $Y, -1, -1, 8, $G_cur)		; �����-�����
EndFunc   ;==>Vicinage

Func Orto($X, $Y, $dX, $dY, $P, $G_cur)
	Local $delta = 10, $aStateSpis, $state, $XX, $YY
	Dim $diagonal, $check_end

	$XX = $X + $dX
	$YY = $Y + $dY
	If $check_end Then Return

	$aStateSpis = CheckStateSpis($XX, $YY)
	If $log Then ConsoleWrite('$aStateSpis:  '& $aStateSpis[0] &':'& $aStateSpis[1] &':'& $aStateSpis[2] &':'& $aStateSpis[3] &':'& $aStateSpis[4] &'    ���:'& $XX &':'& $YY &':'& $P & @CRLF)
	If $aStateSpis[0] Then
		If (Not $aStateSpis[1]) And (Not $aStateSpis[2]) Then
			$state = Reading($XX, $YY)
			If $log Then ConsoleWrite('$state:  '& $state & @CRLF)
			If $state <> 1 Then AddOpen($XX, $YY, $P, $G_cur + $delta)
		Else
			If Not $aStateSpis[1] Then AlreadyOpen($P, $aStateSpis, $G_cur + $delta)
		EndIf
	EndIf
EndFunc   ;==>Orto

Func Diag($X, $Y, $dX, $dY, $P, $G_cur)
	Local $delta = 14, $aStateSpis, $state, $XX, $YY
	Dim $diagonal, $check_end

	$XX = $X + $dX
	$YY = $Y + $dY
	If (Not $diagonal) Or $check_end Then Return

	$aStateSpis = CheckStateSpis($XX, $YY)
	If $log Then ConsoleWrite('$aStateSpis:  '& $aStateSpis[0] &':'& $aStateSpis[1] &':'& $aStateSpis[2] &':'& $aStateSpis[3] &':'& $aStateSpis[4] &'    ���:'& $XX &':'& $YY &':'& $P & @CRLF)
	If $aStateSpis[0] Then
		If (Not $aStateSpis[1]) And (Not $aStateSpis[2]) Then
			$state = Reading($XX, $YY)
			If $log Then ConsoleWrite('$state:  '& $state & @CRLF)
			If $state <> 1 Then
				$state_1 = Reading($X, $YY)
				$state_2 = Reading($XX, $Y)
				IF ($state_1 <> 1) Or ($state_2 <> 1) Then
					If Not($bypass_angle And (($state_1 = 1) Or ($state_2 = 1))) Then
						AddOpen($XX, $YY, $P, $G_cur + $delta)
					EndIf
				EndIf
			EndIf
		Else
			If Not $aStateSpis[1] Then AlreadyOpen($P, $aStateSpis, $G_cur + $delta)
		EndIf
	EndIf
EndFunc   ;==>Diag

Func AlreadyOpen($P, $aStateSpis, $G)
	Local $G_old

	$G_old = $open[$aStateSpis[3]][3]
	If ($G < $G_old) Then
		$H = $open[$aStateSpis[3]][4] - $open[$aStateSpis[3]][3]
		$open[$aStateSpis[3]][2] = $P
		$open[$aStateSpis[3]][3] = $G
		$open[$aStateSpis[3]][4] = $G + $H
	Else
		; �� ��������� � $open
	EndIf
EndFunc   ;==>AlreadyOpen

Func FindBest()
	Local $best = 1, $min

	;If $open[0][0] = 0 Then Return
	$min = $open[1][4]
	For $i = 2 To $open[0][0]
		If $open[$i][4] < $min Then
			$min = $open[$i][4]
			$best = $i
		EndIf
	Next
	Return $best
EndFunc   ;==>FindBest

Func ReadMap($name_file)
	Dim $StartStopInMap, $Start_X, $Start_Y, $End_X, $End_Y, $indexStart, $indexStop, $width, $height
	Local $hFile, $AllText, $aArray

	$hFile = FileOpen(@ScriptDir & '\' & $name_file, 0)
	If $hFile = -1 Then
		MsgBox(4144, "������", "���������� ������� ���� � ������")
		IniWrite ($ini, $section, 'state', '2')
		Exit
	EndIf

    $AllText = FileRead($hFile)
	FileClose($hFile)
    If @error = 1 Then
		MsgBox(4144, "������", "���������� ��������� ���� � ������")
		IniWrite ($ini, $section, 'state', '2')
		Exit
	EndIf

	If $StartStopInMap Then
		If StringInStr($AllText, $indexStart) = 0 Or StringInStr($AllText, $indexStop) = 0 Then
			MsgBox(4144, "������", "� ����� �� ������� ������� ������ �/��� ������")
			IniWrite ($ini, $section, 'state', '2')
			Exit
		EndIf
	Else
		If $Start_X = '' Or $Start_Y = '' Or $End_X = '' Or $End_Y = '' Then
			MsgBox(4144, "������", "�� ������ ������� ������ � ������")
			IniWrite ($ini, $section, 'state', '2')
			Exit
		EndIf
	EndIf

;~ 	MsgBox(4096, '$AllText', $AllText, 10)
;~ 	MsgBox(4096, ' ��������� �������', StringRight($AllText,6))

	$aArray = StringSplit ($AllText, @CRLF , 1)
;~ 	_ArrayDisplay($aArray)

	If $aArray[$aArray[0]] = '' Then
		;MsgBox(4096, '', '��������� ������')
		ReDim $aArray[$aArray[0]]
		$aArray[0] -= 1
	EndIf
;~ 	_ArrayDisplay($aArray)

	$width = StringLen($aArray[1])
	$height = $aArray[0]
;~ 	MsgBox(4096, '', '$width/$height: ' & $width & '/' & $height , 3)

	Global $map[$height][$width]
	For $row = 0 To $height - 1
		$line = StringSplit ($aArray[$row+1], '' , 2)
		For $col = 0 To $width - 1
			$map[$row][$col] = Int($line[$col])
			If $StartStopInMap Then
				If $line[$col] = $indexStart Then
					$Start_X = $col
					$Start_Y = $row
				ElseIf $line[$col] = $indexStop Then
					$End_X = $col
					$End_Y = $row
				EndIf
			EndIf
		Next
	Next
;~ 	MsgBox(4096, '�����  ', $width & ' x ' & $height, 1)
;~ 	MsgBox(4096, '', '�����  ' & $Start_X & ' x ' & $Start_Y & @CRLF & '�����  ' & $End_X & ' x ' & $End_Y)
;~ 	_ArrayDisplay($map)
EndFunc   ;==>ReadMap

Func ReadingIni()
	Dim $name_map, $bypass_angle, $diagonal, $StartStopInMap, $Start_X, $Start_Y, $End_X, $End_Y
	Dim $indexStart, $indexStop, $ini, $timer, $section

	$name_map = IniRead ($ini, $section, 'name_map', 'map.txt')
	$bypass_angle = IniRead ($ini, $section, 'bypass_angle', 'False')
	$diagonal = IniRead ($ini, $section, 'diagonal', 'True')
	$StartStopInMap = IniRead ($ini, $section, 'StartStopInMap', 'True')
	$Start_X = IniRead ($ini, $section, 'Start_X', '')
	$Start_Y = IniRead ($ini, $section, 'Start_Y', '')
	$End_X = IniRead ($ini, $section, 'End_X', '')
	$End_Y = IniRead ($ini, $section, 'End_Y', '')
	$indexStart = IniRead ($ini, $section, 'indexStart', '4')
	$indexStop = IniRead ($ini, $section, 'indexStop', '5')
	$timer = IniRead ($ini, $section, 'timer', 'True')

	If Not FileExists(@ScriptDir & '\' & $name_map) Then
		MsgBox(4144, '��������!', '�� ������ ���� � ������ "' & $name_map & '"' & @CRLF & @CRLF & '��������� ����� ���������', 3)
		Exit
	EndIf

	If StringLower ($bypass_angle) = 'true' Then
		$bypass_angle = True
	Else
		$bypass_angle = False
	EndIf

	If StringLower ($diagonal) = 'true' Then
		$diagonal = True
	Else
		$diagonal = False
	EndIf

	If StringLower ($StartStopInMap) = 'true' Then
		$StartStopInMap = True
	Else
		$StartStopInMap = False
	EndIf

	If StringLower ($timer) = 'true' Then
		$timer = True
	Else
		$timer = False
	EndIf
EndFunc   ;==>ReadingIni

Func Result()
	Dim $End_X, $End_Y
	Local  $X, $Y, $aStateSpis[5], $P, $direction, $preresult[100][5] = [[0]]
	If $log Then ConsoleWrite('Result' & @CRLF)
	;_ArrayDisplay($open)
	;_ArrayDisplay($close)

	$X = $End_X
	$Y = $End_Y
	$aStateSpis = CheckStateSpis($X, $Y)
	; Filed, closed, opened, index opened, index closed
	;If $log Then ConsoleWrite('$aStateSpis:  '& $aStateSpis[0] &':'& $aStateSpis[1] &':'& $aStateSpis[2] &':'& $aStateSpis[3] &':'& $aStateSpis[4] &'    ���:'& $X &':'& $Y & @CRLF)
	If $aStateSpis[1] Then
		$P = $close[$aStateSpis[4]][2]
	Else
		$P = $open[$aStateSpis[3]][2]
	EndIf


	While $P > 0
		;If $log Then ConsoleWrite('$P =  '& $P & @CRLF)
		Switch $P
			Case 1
				$direction = "�����"
				$Y += 1
			Case 2
				$direction = "�����-������"
				$Y += 1
				$X -= 1
			Case 3
				$direction = "������"
				$X -= 1
			Case 4
				$direction = "����-������"
				$Y -= 1
				$X -= 1
			Case 5
				$direction = "����"
				$Y -= 1
			Case 6
				$direction = "����-�����"
				$Y -= 1
				$X += 1
			Case 7
				$direction = "�����"
				$X += 1
			Case 8
				$direction = "�����-�����"
				$Y += 1
				$X += 1
		EndSwitch

		$preresult[0][0] += 1
		If $preresult[0][0] > UBound($preresult, 1) - 1 Then
			ReDim $preresult[$preresult[0][0] + 100][5]
		EndIf
		$preresult[$preresult[0][0]][0] = $X
		$preresult[$preresult[0][0]][1] = $Y
		$preresult[$preresult[0][0]][2] = $P
		$preresult[$preresult[0][0]][3] = $direction
		$preresult[$preresult[0][0]][4] = $X & '/' & $Y & '  ' & $P & ' - ' & $direction

		$aStateSpis = CheckStateSpis($X, $Y)
		If $log Then ConsoleWrite('$aStateSpis:  '& $aStateSpis[0] &':'& $aStateSpis[1] &':'& $aStateSpis[2] &':'& $aStateSpis[3] &':'& $aStateSpis[4] &'    ���:'& $X &':'& $Y & @CRLF)
		If $aStateSpis[1] Then
			$P = $close[$aStateSpis[4]][2]
		Else
			$P = $open[$aStateSpis[3]][2]
		EndIf
	WEnd
	If $log Then _ArrayDisplay($preresult, '$preresult')

	; reverse $preresult ==>> $result
	Local $result[$preresult[0][0]][5], $result_min[$preresult[0][0]]

	$result[0][0] = $preresult[0][0]
	For $i = 1 To $preresult[0][0]
		For $j = 0 To 4
			$result[$i-1][$j] = $preresult[$preresult[0][0] - $i + 1][$j]
		Next
		$result_min[$i-1] = $preresult[$preresult[0][0] - $i + 1][2]
	Next
	If $log Then _ArrayDisplay($result, '$result')
	;_ArrayDisplay($result_min, '$result_min')
	Return $result_min
EndFunc   ;==>Result

Func WriteResult($result_min)
	Dim $name_map, $section
	Local $hFile, $text
	If $log Then ConsoleWrite('WriteResult' & @CRLF)

	If UBound($result_min) = 0 Then Return
	$hFile = FileOpen (@ScriptDir & '\result_' & $name_map, 2)

	$text = String($result_min[0])
	For $i = 1 To UBound($result_min) - 1
		$text &= @CRLF & $result_min[$i]
	Next
	FileWrite($hFile, $text)
	FileClose($hFile)
	IniWrite ($ini, $section, 'state', '1')
EndFunc   ;==>WriteResult

Exit



$a = True
MsgBox(4096, '��� ����������', VarGetType($a))

If @Compiled Then
	; ������ �����������, ���� ��� �����-������ ���������� ��������� ������
	If $CmdLine[0] = 0 Then Exit(1)
	$section = $CmdLine[1]
Else
	;$section = 'Window'
	;$section = 'Window_select'
	$section = 'Window_number'
EndIf
